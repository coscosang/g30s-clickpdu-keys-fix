import time
import hid
from pynput.keyboard import Key, Controller
import usb.util

# Получаем список всех USB-устройств
devices = usb.core.find(find_all=True)

# Проходимся по каждому устройству и выводим его Vendor ID и Product ID
for device in devices:
    print(device)


# Найдем HID-устройство по Vendor ID и Product ID
vendor_id = 0x25A7  # Замените на свой Vendor ID
product_id = 0x0026  # Замените на свой Product ID

device = hid.Device(vendor_id, product_id)


try:
    # Основной цикл мониторинга
    print('Start')
    while True:
        data = device.read(32)  # Чте1ние данных с устройства (размер буфера можно изменить)
        print("Получено: ", data, time.time())
        if data == b'\x01$\x02':
            keyboard = Controller()

            # Эмулируем нажатие клавиши "Esc"
            keyboard.press(Key.esc)
            keyboard.release(Key.esc)


                # Ждем некоторое время (по желанию)
            print('!!!!!!!!!!!!')
except TimeoutError:
    print('!!!!!!!')
    pass
finally:
    print('Close')
    device.close()
